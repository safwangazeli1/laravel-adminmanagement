<?php

namespace App\Http\Controllers;

use App\Models\Plan;
use App\Models\Payment;
use Illuminate\Support\Str;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Mail\ThankyouPaymentEmail;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SignUpController extends Controller
{
 
    public function index(){

       $plans = Plan::all();

        return view('signup.index',[
            'plans' => $plans
        ]);
    }

    public function review($id){

        $plan =  Plan::findOrFail($id);
        $user = User::find( Auth::id() );

        return view('signup.review',[
            'plan' => $plan,
            'user' => $user
        ]);
    }

    public function thankyou(Request $request, $payment_gateway){

        $result = $this->payment_process($request, $payment_gateway);
        
        return view('signup.thankyou', $result);
        
    }


    public function payment_callback(Request $request, $payment_gateway ) {
    
        $result = $this->payment_process($request, $payment_gateway);

        return true;
    
    }

    public function payment_process(Request $request, $payment_gateway) {


        $verified = false;
        $is_paid = false;
        $is_processed = false;

        switch($payment_gateway){
            case 'billplz' :
                break;
            case 'ipay88' :
                break;
            case 'securepay' : 

            $sp = new SecurePay;

            if(isset($request->checksum)){
                $verified = $sp->verify($request);
            }
            
            // $verified = $sp->verify($request);

            if($verified) {
                $payment = Payment::where('order_number', $request->order_number)->first();
         
                $transaction_data = json_decode( $payment->transaction_data ); 
                // transaction_data->duration


                if(($request->payment_status) == 'true' &&  ($payment->status != 'paid')){
                    $payment->status = 'paid';
                    $is_paid = true;
                    $payment->save();
                
                //Update subscirption recoed
                
                $subscription = Subscription::firstOrCreate([
                    'user_id' => $payment->user_id
                ]);

                // Pakai PHP biasa
                // $today = date('Y-m-d H:i:s'); -

                // Pakai Carbon
                $today = Carbon::now(); //utk dptkan tarikh hari semasa
                // $expire_at = new Carbon($subscription->expired_at); cara lain kalau tak pakai casting

                if(($subscription->expired_at === null) || ($subscription->expired_at->lessThan($today) )){
                    $subscription->expired_at = $today->addDays($transaction->duration);
                }else {
                    $subscription->expired_at = $subscription->expired_at->addDays($transaction_data->duration);    
                }

                $subscription->last_payment_id = $payment->id;

                $subscription->save();

                $user = User::find($payment->user_id);
                
                $data = [
                    'name' => $user->name,
                    'expire_at' => $subscription->expired_at,
                    'amount' => 'RM'.$transaction_data->transaction_amount
                ];

                Mail::to($user->email)->send( new ThankyouPaymentEmail($data) );



                $is_processed = true;

                }

            }else{
                // non verified payment ---  to log attempt 
            }

            break;
        }

        return ([
            'verified' => $verified,
            'is_paid' => $is_paid,
            'is_processed' => $is_processed
        ]);
    }

    public function go(Request $request, $payment_gateway){
       
        $user = User::find( Auth::id() );
        $plan = Plan::find($request->plan_id);

        switch($payment_gateway){

        case 'billplz' :
            break;
        case 'ipay88' :
            break;
        case 'securepay' :

            $sp = new SecurePay;
            $payment_data = [
                'buyer_name' => $user->name,
                'buyer_email' => $user->email,
                'buyer_phone' => $request->buyer_phone,
                'callback_url' => route('signup.callback', 'securepay'), //dyanamic
                'order_number' => Str::random(10),
                'product_description' => 'Bayaran Pelan'.$plan->name.'untuk'.$user->name,
                // 'redirect_url' => 'http://bootcamp.test/signup/thankyou/securepay', ini cara hardcode 
                'redirect_url' => route('signup.thankyou', 'securepay'),  //dyanamic
                'transaction_amount' => number_format($plan->price / 100, 2, '.', ''),
                'duration' => $plan->duration,
                'plan_id' => $plan->id
            ];

            Payment::create([
                'payment_gateway' => 'securepay',
                'order_number' => $payment_data['order_number'],
                'amount' => $payment_data['transaction_amount'],
                'transaction_data' => json.encode($payment_data),
                'status' => 'pending',
                'user_id' => Auth::id()
            ]);

            $sp->process($payment_data);

            break;
        }
        
    }

}
