<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function index(){

        $today = Carbon::now();
        $month_ago = Carbon::now()->subDays(30);

        $sdb = DB::select('select
        DATE_FORMAT(created_at, "%Y-%m-%d") AS sale_date,
        SUM(amount) AS total,
        COUNT(id) AS sales
        FROM payments
        where created_at >= ?
        and created_at <= ?
        and status = "paid"
        GROUP BY sale_date
        ORDER BY sale_date asc'
    ,[
        $month_ago->toDateString().'00:00:00',
        $today->toDateString().'23:59:59'
    ]);

    $period = CarbonPeriod::create( $month_ago, $today);
        
    $sale_list = Payment::where('created_at', '>=', $month_ago->toDateString().'00:00:00')
    ->where('created_at', '<=', $today->toDateString().'00:00:00')
    ->where('status', '=', 'paid')
    ->with('user')
    ->get();
    
    $sales_data = [];
    foreach($sdb as $sd){
        $sales_data[$sd->sale_date] = $sd;
    }
        return view('admin.payment.index', [
            'period' => $period,
            'sales_data' => $sales_data,
            'sale_list' => $sale_list
        ]);
    }
}
