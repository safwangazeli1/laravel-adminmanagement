<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Payment extends Model
{
    use HasFactory;

    
    protected $fillable = [
        'payment_gateway',
        'order_number',
        'amount',
        'user_id',
        'transaction_data',
        'status',
        'created_at',
        'deleted_at'
    ];
    
    public function user(){
        return $this->belongsTo( User::class );
    }
}
