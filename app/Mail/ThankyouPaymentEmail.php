<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ThankyouPaymentEmail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct( Array $data )
    {
    $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.thankyoupayment')
        ->subject($this->data['name'].'Terima Kasih, Akuan Anda Telah diperbaharui')
        ->with($this->data);
    }
}
