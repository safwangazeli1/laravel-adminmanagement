<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ExpireSoonEmail extends Mailable
{
    use Queueable, SerializesModels;


    public function __construct( Array $data )
    {
    $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.expiresoon')
         ->subject($this->data['name'].', Akaun Anda Bakal Luput')
         ->with($this->data);
    }
}
