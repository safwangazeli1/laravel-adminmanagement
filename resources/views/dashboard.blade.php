@extends('layout.private')

@section('content')
    @if (session('status'))
        <div>{{ session('status') }}</div>
    @endif

    <div>You are logged in!</div>

    @if( Auth::user()->hasVerifiedEmail() )
      You are not verify email
    @else
        You are not verify email yet

        <form action="/email/verification-notification" method="post">
            @csrf
            <button type="submit">Resend Email Verification</button>
        </form>
    @endif

    @can('is-expired')
    <div class="alert alert-danger">Keahlian Anda Telah Luput, Sila perbaharui keahlian di <a href="/signup"> halaman sign up</a> </div>
    @endcan

    @can('is-active')
    <div class="alert alert-success"><strong>Expire At: </strong> Keahlian akan luput dalam {{ Auth::user()->subscription->expire_at->diffInDays() }} Hari</div>
    @endcan

    <form method="POST" action="{{ route('logout') }}">
        @csrf

        <button type="submit">
            {{ __('Logout') }}
        </button>
    </form>

    <hr>

   
@endsection
