@extends('layout.private')

@section('content')

<h1 class="h3 mb-2 text-gray-800">Tambah Pengguna Baru</h1>

<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Isi Maklumat Pengguna Baru</h6>
    </div>
    <div class="card-body">
        <form action="{{ route('user.store') }}" method="post">
            @csrf

            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name">
                    @error('name')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                    {{$message}}
                    </div>
                    @enderror
                    
                </div>
            </div>

            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email">
                    @error('email')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                    {{$message}}
                    </div>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                    @error('password')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                    {{$message}}
                    </div>
                    @enderror
                </div>
            </div>

            <div class="mb-3 row">
                <label class="col-sm-2 col-form-label">Confirm Password</label>
                <div class="col-sm-10">
                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation">
                    @error('password_confirmation')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                    {{$message}}
                    </div>
                    @enderror
                </div>
            </div>


            
            <a href="/admin/user" class="btn btn-lg btn-secondary">Batal</a>
            <button class="btn btn-lg btn-primary">Hantar</button>
        </form>
    </div>
</div>


@endsection