@extends('layout.public')

@section('content')
    @if (session('status'))
        <div>
            {{ session('status') }}
        </div>
    @endif

    @if ($errors->any())
        <div>
            <div>{{ __('Whoops! Something went wrong.') }}</div>

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" class="user" action="{{ route('login') }}">
        @csrf

        <div class="form-group">
            <!-- <label>{{ __('Email') }}</label> -->
            <input type="email" class="form-control form-control-user" name="email" aria-describedby="emailHelp" value="{{ old('email') }}" required autofocus  placeholder="Enter Email Address..." />
        </div>

        <div class="form-group">
            <!-- <label>{{ __('Password') }}</label> -->
            <input type="password" class="form-control form-control-user" name="password" required autocomplete="current-password"  placeholder="Password" />
        </div>

        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" name="remember" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div>

                                        <div>
                                            <button type="submit" class="btn btn-primary btn-user btn-block">
                                            {{ __('Login') }}
                                            </button>
                                        </div>
 

                                        <hr>

        <!-- <div>
            <label>{{ __('Remember me') }}</label>
            <input type="checkbox" name="remember">
        </div> -->

        @if (Route::has('password.request'))

        <div class="text-center">
             <a class="small" href="{{ route('password.request') }}">
                 Forgot your Password?
             </a>
        </div>
            <!-- <a class="small" href="{{ route('password.request') }}">
                {{ __('Forgot your password?') }}
            </a> -->
        @endif

        <div class="text-center">
            <a class="small" href="/register">Create an Account!</a>
         </div>


      
    </form>
@endsection
