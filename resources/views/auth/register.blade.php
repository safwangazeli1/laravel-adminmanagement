@extends('layout.public')

@section('content')
    @if ($errors->any())
        <div>
            <div>{{ __('Whoops! Something went wrong.') }}</div>

            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" class="user" action="{{ route('register') }}">
        @csrf

        <!--BARU  -->
        <div class="form-group row">
                                    <div class="col-sm-12 mb-1 mb-sm-0">
                                        <input type="text" class="form-control form-control-user" name="name" value="{{ old('name') }}" required autofocus autocomplete="name" id="exampleFirstName"  placeholder="Name">
                                    </div>
        </div>
        <div class="form-group">
                                    <input type="email" class="form-control form-control-user" id="exampleInputEmail" name="email" value="{{ old('email') }}" placeholder="Email Address" required >
        </div>

        <div class="form-group row">
                                    <div class="col-sm-6 mb-3 mb-sm-0">
                                        <input type="password" class="form-control form-control-user" id="exampleInputPassword" name="password" required autocomplete="new-password" placeholder="Password">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="password" class="form-control form-control-user" id="exampleRepeatPassword"  name="password_confirmation" required autocomplete="new-password" placeholder="Repeat Password">
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Register Account
                                </button>

                                <hr>
                                <div class="text-center">
                                     <!-- <a class="small" href="login.html">Already have an account? Login!</a> -->
                                     <a class="text-center" href="{{ route('login') }}" >
                                      {{ __('Already have an account? Login!') }}
                                     </a>
                                </div>
                              
                               
        </div>

        <!-- LAMA -->
        <!-- <div>
            <label>{{ __('Name') }}</label>
            <input type="text" name="name" value="{{ old('name') }}" required autofocus autocomplete="name" />
        </div> -->

        <!-- <div>
            <label>{{ __('Email') }}</label>
            <input type="email" name="email" value="{{ old('email') }}" required />
        </div> -->

        <!-- <div>
            <label>{{ __('Password') }}</label>
            <input type="password" name="password" required autocomplete="new-password" />
        </div>

        <div>
            <label>{{ __('Confirm Password') }}</label>
            <input type="password" name="password_confirmation" required autocomplete="new-password" />
        </div> -->

        <!-- <a href="{{ route('login') }}">
            {{ __('Already registered?') }}
        </a> -->

        <!-- <div>
            <button type="submit">
                {{ __('Register') }}
            </button>
        </div> -->
    </form>
@endsection
