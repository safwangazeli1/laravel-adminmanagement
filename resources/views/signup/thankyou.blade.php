@extends(layout.public)

@section('content')

<h3>Terima Kasih</h3>

@if($verified)
    <p>Terima kasih atas pembayaran keahlian yang telah dibuat, sila log in untuk menyemak keahlian yang baru </p>
@else
    <p>Pembayaran anda akan diproses, Sila Login untuk menyemak status keahlian.</p>
@enif


<a href="/login" class="btn btn-primary">Login</a>
@endsection