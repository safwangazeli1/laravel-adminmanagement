@extends('layout.public')

@section('content')

<h2>Review</h2>

<p>Name: {{$user->name}}</p>
<p>Name: {{$user->email}}</p>
<p>Anda Akan membuat Bayaran Untuk pelan berikut</p>

<div class="card shadow">
    <div class="card-body mt-3">
        <h5>Pelan {{$plan->name}}</h5>
        <p>Tempoh: {{$plan->duration}}</p>
        <p>{{ $plan->money_price }}</p>
    </div>
</div>

<h4 class="mt-2">Kaedah Bayaran</h4>

<form action="{{ route('signup.go', 'securepay') }}" method="post">
    
    @csrf
    <input type="hidden" name="plan_id" value="{{ $plan->id }}">
    <label for="buyer_phone">Nombor Telefon</label>
    <input type="text" name="buyer_phone" class="form-control mb-3">

    <button type="submit "href="/signup/go/securepay/{{$plan->id}}"class="btn btn-primary">Securepay </button>

</form>
@endsection