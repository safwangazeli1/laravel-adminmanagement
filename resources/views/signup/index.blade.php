@extends('layout.public')

@section('content')


<h2>Pendaftaran</h2>
<p>Sila Pilh Pelan Langanan</p>

<div class="row">
    @foreach($plans as $plan)
    <div class="col">
        <div class="card shadow">
            <div class="card-body">
                <h5>{{ $plan->name}}</h5>
                <h6>{{ $plan->money_price }}</h6>
                <p><strong>Tempoh:</strong> {{ $plan->duration}} Hari </p>


                @auth 
                    <a href="/signup/review/{{ $plan->id}}" class="plan-button btn btn-warning">
                        Langgan Sekarang
                    </a>
                @endauth

                @guest
                <button data-plan-id="{{$plan->id}}" class="plan-button btn btn-warning" data-toggle="modal" data-target="#loginOrRegister-model">
                    Langgan Sekarang
                </button>
                @endguest
            </div>
        </div>
    </div>
    @endforeach
</div>


<!-- Modal -->
<div class="modal fade" id="loginOrRegister-model" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sila Login atau Daftar </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Sila Login Atau Daftar Sebelum Membuat Bayaran</p>

    
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login-form" role="tab"
                            aria-controls="home" aria-selected="true">Login</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="register-tab" data-toggle="tab" href="#register-form" role="tab"
                            aria-controls="profile" aria-selected="false">Daftar</a>
                    </li>
                </ul>

                <!-- LOGIN FORM -->
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="login-form" role="tabpanel" aria-labelledby="home-tab">
                        <!-- <h4>Login</h4> -->


                        <form method="POST" class="user mt-3" id="login-form-form" action="{{ route('login') }}">
                            @csrf


                            <div class="alert alert-danger d-none" id="login-alert">
                                Email atau Katalaluan tidak sah.
                            </div>


                            <div class="form-group">
                                <!-- <label>{{ __('Email') }}</label> -->
                                <input type="email" id="login-form-email" class="form-control form-control-user" name="email"
                                    aria-describedby="emailHelp" value="{{ old('email') }}" required autofocus
                                    placeholder="Enter Email Address..." />
                            </div>

                            <div class="form-group">
                                <!-- <label>{{ __('Password') }}</label> -->
                                <input type="password" id="login-form-password" class="form-control form-control-user" name="password" required
                                    autocomplete="current-password" placeholder="Password" />
                            </div>
                            <!-- 
        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" name="remember" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                    Me</label>
                                            </div>
                                        </div> -->

                            <div>
                                <button type="button" id="login-button" class="btn btn-primary btn-user btn-block">
                                    {{ __('Login') }}
                                </button>
                            </div>


                            <hr>


                            @if (Route::has('password.request'))

                            <div class="text-center">
                                <a class="small" href="{{ route('password.request') }}">
                                    Forgot your Password?
                                </a>
                            </div>
                            @endif

                            <div class="text-center">
                                <a class="small" href="/register">Create an Account!</a>
                            </div>



                        </form>
                    </div>

                    <!-- REGISTER FORM -->
                    <div class="tab-pane fade" id="register-form" role="tabpanel" aria-labelledby="profile-tab">

                        <form method="POST" class="user mt-5" action="{{ route('register') }}">
                            @csrf

                            <div class="alert alert-danger d-none" id="register-alert">
                                Sila semak borang pendaftaran.
                            </div>

                            <!--BARU  -->
                            <div class="form-group row">
                                <div class="col-sm-12 mb-1 mb-sm-0">
                                    <input type="text" class="form-control form-control-user" name="name"
                                        value="{{ old('name') }}" id="register-form-name" required autofocus autocomplete="name"
                                        id="exampleFirstName" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control form-control-user" id="exampleInputEmail"
                                    name="email" value="{{ old('email') }}" id="register-form-email" placeholder="Email Address" required>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6 mb-3 mb-sm-0">
                                    <input type="password" class="form-control form-control-user"
                                        id="exampleInputPassword" name="password" id="register-form-password"required autocomplete="new-password"
                                        placeholder="Password">
                                </div>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control form-control-user"
                                        id="exampleRepeatPassword" name="password_confirmation" id="register-form-password-confirmation" required
                                        autocomplete="new-password" placeholder="Repeat Password">
                                </div>
                            </div>

             
                            <button type="button" id="register-button" class="btn btn-primary btn-user btn-block">
                                Register Account
                            </button>
                       
                            <hr>
                            <div class="text-center">
                                <!-- <a class="small" href="login.html">Already have an account? Login!</a> -->
                                <a class="text-center" href="{{ route('login') }}">
                                    {{ __('Already have an account? Login!') }}
                                </a>
                            </div>


                    </div>
                    </form>

                </div>



            </div>


        </div>

    </div>
</div>
</div>

@endsection

@section('page-js')
<script src="/js/signup.js"></script>
@endsection
